# Dyno Bug List

Add any issues here! Check for duplicates first, however it's better to add a duplicated issue than to not add one, so don't feel pressed! Both bugs and small improvements (use label QoL) can be added to the issues. Full features don't belong here.

In case you talked to someone on Discord (Gin, chipped or Noob) about an issue and they told that they were going to fix, feel free to assign them to the issue. We'll go through the issues and fix what's needed to be fix or request more information!

Once a bug is fixed, we will add the "Awaiting Deploy" tag to it. Once it's deployed and confirmed working, we will close the issue.

Try to add as much information as possible, issues support links, images and markdown. 

Once you request access, shoot a DM to Gin to get added to the repository. Anyone can open issues, however only people with access can assign labels to them.

In case you guys have any questions, don't be afraid to ask!
